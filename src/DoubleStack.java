import java.util.*;

/** Stack manipulation.
 * @since 1.8
 */
public class DoubleStack {

   public static void main (String[] argum) {
   }

   private final LinkedList<Double> stack;

   DoubleStack() {
      stack = new LinkedList<>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      DoubleStack o = new DoubleStack();
      if (stEmpty()) return o;
      for (int i =stack.size(); i>0;i--)
         o.push(stack.get(i-1));
      return o;
   }

   public boolean stEmpty() {
      return stack.size() == 0;
   }

   public void push (double a) {
      this.stack.push(a);
   }

   public double pop() {
      if (stack.size() < 1) throw new RuntimeException("Stack must have at least 1 element.");
      return this.stack.pop();
   } // pop

   public void op(String s) {
      if (stack.size() < 2) throw new RuntimeException("Stack must have at least 2 elements. Input: " + s);
      double a = pop();
      double b = pop();
      if (s.equals("+")) {
         push(b + a);
         return;
      }
      if (s.equals("-")) {
         push(b - a);
         return;
      }
      if (s.equals("/")) {
         push(b / a);
         return;
      }
      if (s.equals("*")) {
         push(b * a);
         return;
      }
      throw new RuntimeException("Illegal symbol: " + s);
   }
  
   public double tos() {
      if (stack.size() < 1) throw new RuntimeException("Stack must have at least 1 element.");
      return stack.getFirst();
   }

   @Override
   public boolean equals (Object o) {
      if (o.getClass() != this.getClass()) return false;
      if (stEmpty() && ((DoubleStack) o).stEmpty()) return true;
      if (stack.size() != ((DoubleStack) o).stack.size()) return false;
      for (int i=0;i<this.stack.size();i++)
         if (!this.stack.get(i).equals(((DoubleStack) o).stack.get(i))) return false;
      return true;
   }

   @Override
   public String toString() {
      if (stEmpty()) return "empty";
      StringBuffer b = new StringBuffer();
      for (int i = stack.size(); i > 0; i--)
         b.append(stack.get(i-1)).append(" ");
      return b.toString();
   }

   public static double interpret (String pol) {

      DoubleStack s = new DoubleStack();
      String[] str = pol.trim().split(" +");
      if (str.length < 1) throw new RuntimeException("Empty Expression");
      for (String s1 : str) {
         if (s1.equals("+") || s1.equals("-") || s1.equals("/") || s1.equals("*")) {
            if (s.stack.size() < 2) throw new RuntimeException("Cannot perform "+ s1 + "in expression: " + pol);
            s.op(s1);
         } else if (s1.equals("SWAP")) {
            if (s.stack.size() < 2) throw new RuntimeException("Cannot perform " + s1 + " in expression" + pol);
            s.swap();
         } else if (s1.equals("DUP")) {
            if (s.stack.size() < 1) throw new RuntimeException("Cannot perform " + s1 + " in expression" + pol);
            s.dup();
         } else if (s1.equals("ROT")) {
            if (s.stack.size() < 3) throw new RuntimeException("Cannot perform " + s1 + " in expression" + pol);
            s.rot();
         } else {
            try {
               s.push(Double.parseDouble(s1));
            } catch (NumberFormatException e) {
               throw new RuntimeException("Illegal symbol " + s1 + " in expression: " + pol);
            }
         }
      }
      if (s.stack.size() > 1) throw new RuntimeException("Too many numbers in expression: " + pol);
      return s.tos();
   }

   public void swap() {
      if (stack.size() < 2) throw new RuntimeException("Too few elements to perform SWAP expression");
      double a = pop();
      double b = pop();
      push(a);
      push(b);
   }

   public void dup() {
      if (stack.size() < 1) throw new RuntimeException("Too few elements to perform DUP expression");
      double a = tos();
      push(a);
   }

   public void rot() {
      if (stack.size() < 3) throw new RuntimeException("Too few elements to perform ROT expression");
      double c = pop();
      double b = pop();
      double a = pop();
      push(b);
      push(c);
      push(a);
   }

}
